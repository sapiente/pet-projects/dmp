<?php

namespace App\Presenters;

/**
 * Class BasePresenter
 * @package App\Presenters
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class BasePresenter extends \Nette\Application\UI\Presenter
{


	public function createComponentMenu()
	{
		return new \App\Components\MenuControl($this->user);
	}
}