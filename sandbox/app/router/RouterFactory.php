<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;

		$admin = new RouteList();
		$admin[] = new Route('admin', 'Admin:Homepage:default');
		$admin[] = new Route('admin/nastaveni', 'Admin:Account:edit');
		$router[] = $admin;

		$public = new RouteList();
		$public[] = new Route('? year=<year>', 'Public:LGWork:showAll');
		$public[] = new Route('prihlaseni', 'Public:Sign:in');
		$public[] = new Route('odhlaseni', 'Public:Sign:out');
		$router[] = $public;

		$user = new RouteList();
		$user[] = new Route('user', 'User:LGWork:showAll');
		$user[] = new Route('dmp/nova', 'User:LGWork:add');
		$user[] = new Route('registrace', 'User:Account:register');
		$user[] = new Route('uzivatel/nastaveni', 'User:Account:edit');
		$user[] = new Route('check/nickname/<nickname>', 'User:Account:CheckUniqueNickname');
		$router[] = $user;

//		$router[] = new Route('prihlaseni', 'Sign:in');
//		$router[] = new Route('odhlaseni', 'Sign:out');
//
//		$router[] = new Route('lgWorks/year/<year>', 'LGWork:getLGWorks');
//		$router[] = new Route('dmp/nova', 'LGWork:add');
//
//		$router[] = new Route('account/check/nickname/<nickname>', 'Account:CheckUniqueNickname');
//		$router[] = new Route('registrace', 'Account:register');
//		$router[] = new Route('nastaveni/uctu', 'Account:edit');
//
//		$router[] = new Route('<presenter>/<action>[/<id>]', 'LGWork:showAll');
		return $router;
	}

}
