<?php

namespace App\PublicModule\Presenters;

use Nette;
use App\Forms\SignFormFactory;


class SignPresenter extends BasePresenter
{
	/** @var SignFormFactory @inject */
	public $factory;


	public function actionIn()
	{
		if($this->user->isLoggedIn()) {
			$this->actionOut();
		}
	}

	public function renderIn()
	{}

	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
		$form = $this->factory->create();
		$form->onSuccess[] = function ($form) {
			if($this->user->isInRole('admin')) {
				$this->redirect(':Admin:Homepage:default');
			} else {
				$this->redirect(':User:LGWork:showAll');
			}

		};
		return $form;
	}


	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('Byl jsi odhlášen.');
		$this->redirect('in');
	}

}
