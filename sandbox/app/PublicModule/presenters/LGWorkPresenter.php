<?php

namespace App\PublicModule\Presenters;

use App\Model\LGWorkModel;
use Nette;
use App\Model;
use Nette\Utils\DateTime;

/**
 * Class LGWorkPresenter
 * @package App\Presenters
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class LGWorkPresenter extends BasePresenter
{
	/** @var  LGWorkModel */
	protected $_LGWorkModel;
	/** @var  int */
	protected $_year;

	/**
	 * LGWorkPresenter constructor.
	 * @param LGWorkModel $LGWorkModel
	 */
	public function __construct(LGWorkModel $LGWorkModel)
	{
		$this->_LGWorkModel = $LGWorkModel;
		$this->_year = (new DateTime())->format('Y');
	}


	public function actionShowAll($year)
	{
		if($this->isAjax()) {
			$LGWorkContainer = $this->_LGWorkModel->getLGWorkContainerWithWorks($year);
			$this->sendJson($LGWorkContainer->getLGWorks());
		}
	}

	public function renderShowAll($year)
	{
		if(is_null($year)) $year = (new DateTime())->format('Y');
		$this->template->year = $year ;
		$this->template->LGWorks = $this->_LGWorkModel->getAllWork($year);
	}

	public function renderAdd()
	{}


}
