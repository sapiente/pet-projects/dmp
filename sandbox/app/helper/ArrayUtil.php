<?php

namespace App\Helper;

/**
 * Class ArrayUtil
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class ArrayUtil
{


	/**
	 * This function will convert nested Nette\Utils\ArrayHash into simple array;
	 * @param \Nette\Utils\ArrayHash $hash
	 * @return array
	 */
	public static function convertArrayHashToArray(\Nette\Utils\ArrayHash $hash)
	{
		$array = [];
		foreach ($hash as $key => $value) {
			$array[$key] = ($value instanceof \Nette\Utils\ArrayHash) ? static::convertArrayHashToArray($value) : $value;
		}

		return $array;
	}

	/**
	 * Filter array keys by given needles.
	 * @param array $haystack Array to be filtered.
	 * @param array $needles Searched keys.
	 * @param bool|false $removeAfter If is true, found key will be removed from given haystack.
	 * @return array Filtered keys value.
	 */
	public static function filterArrayKeys(array &$haystack, array $needles, $removeAfter = false)
	{
		$output = [];
		foreach ($haystack as $key => $value) {
			if(in_array($key, $needles)) {
				$output[$key] = [$value];
				if($removeAfter) unset($haystack[$key]);
			}

			if(is_array($value)) array_merge($output, static::filterArrayKeys($value, $needles, $removeAfter));
		}

		return $output;
	}
}