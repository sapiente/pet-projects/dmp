<?php

namespace App\Helper;

/**
 * Class DBConstants
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class DBConstants
{
	const
		LGWORK_TABLE = 'long_graduation_work',
		ACCOUNT_TABLE = 'account',
		ACCOUNT_SETTINGS_TABLE = 'account_config',
		STUDENT_TABLE = 'student',
		TEACHER_TABLE = 'teacher';

}