<?php


namespace App\Forms;

use Nette\Object;

/**
 * Class AccountFormFactory
 * @package App\Forms
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
abstract class AccountFormFactory extends Object
{
	/**
	 * Create registration form
	 * @return Form
	 */
	abstract public function createInsertForm();

	/**
	 * Create edit form
	 * @return Form
	 */
	abstract public function createEditForm();

	/**
	 * Create form with all possibly inputs.
	 * @return Form
	 */
	abstract protected function createFullForm();
}