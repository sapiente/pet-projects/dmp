<?php


namespace App\Forms;


use Nette\Application\UI\Form;
use Nette\DI\Container;

class SetPasswordFormFactory extends \Nette\Object
{
	/** @var  Container $_container */
	protected $_container;

	/**
	 * SetPasswordFormFactory constructor.
	 * @param Container $container
	 */
	public function __construct(Container $container)
	{
		$this->_container = $container;
	}


	public function create()
	{
		$form = new Form();
		$form->addText('password', 'Heslo')
			->setRequired('Heslo je povinné')
			->addCondition(Form::MIN, 6);
	}
}