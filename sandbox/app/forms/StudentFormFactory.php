<?php


namespace App\Forms;
use App\Model\AccountModel;
use App\Model\AccountSettingsModel;
use App\Model\StudentModel;
use App\Model\UserManager;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Security\User;
use Nette\Utils\Strings;


class StudentFormFactory extends AccountFormFactory
{

	/** @var UserManager */
	protected $_userModel;
	/** @var AccountSettingsModel $_accountSettingsModel */
	protected $_accountSettingsModel;
	/** @var StudentModel $_studentModel */
	protected $_studentModel;
	/** @var  User $_user */
	protected $_user;

	/**
	 * AccountFormFactory constructor.
	 * @param User $user
	 * @param AccountModel $userModel
	 * @param AccountSettingsModel $accountSettingsModel
	 * @param StudentModel $studentModel
	 */
	public function __construct(User $user, AccountModel $userModel, AccountSettingsModel $accountSettingsModel, StudentModel $studentModel )
	{
		$this->_user = $user;
		$this->_userModel = $userModel;
		$this->_accountSettingsModel = $accountSettingsModel;
		$this->_studentModel = $studentModel;
	}

	/**
	 * Create form with all possibly inputs.
	 * @return Form
	 */
	protected function createFullForm()
	{
		$form = new Form();
		$form->addText('email', 'Email')
			->setRequired('Uživatelské jméno je povinné');
		$form->addText('password', 'Heslo')
			->setRequired('Heslo je povinné!')
			->addCondition(Form::MIN_LENGTH, 6);
		$form->addText("student_name", "Jméno")
			->setRequired('Jméno je povinné');
		$form->addText("student_surname", "Přijmení")
			->setRequired('Přijmené je povinné');
		$form->addText("student_class", "Třída")
			->setRequired('Třída je povinná');
		$form->addText("student_email", "Email")
			->setType("email");
		$form->addText('role','')
			->setType('hidden')
			->setValue('student');
		$form->addCheckbox('settings_send_email', 'Emailem');
		$form->addCheckbox('settings_send_notification', 'Na účet');

		return $form;
	}


	/**
	 * Return registration form.
	 * @return Form
	 */
	public function createInsertForm()
	{
		$form = $this->createFullForm();
		$form->onSuccess[] = array($this, 'insertFormSucceed');

		return $form;
	}

	/**
	 * Create edit form
	 * @return Form
	 */
	public function createEditForm()
	{
		$form = $this->createFullForm();
		$form->onSuccess[] = array($this, 'editFormSucceed');

		return $form;
	}

	/**
	 * Create edit form for student
	 * @return Form
	 */
	public function createEditFormForStudent()
	{
		$form = new Form();
		$form->addText('password', 'Heslo')
			->setRequired('Heslo je povinné!')
			->addCondition(Form::MIN_LENGTH, 6);
		$form->addText("student_email", "Email")
			->setType("email");
		$form->addCheckbox('settings_send_email', 'Emailem');
		$form->addCheckbox('settings_send_notification', 'Na účet');
		$form->onSuccess[] = array($this, 'editFormSucceed');
		return $form;
	}

	/**
	 * @param Form $form
	 * @param \Nette\Utils\ArrayHash $values
	 */
	public function insertFormSucceed(Form $form, $values)
	{
		if(!$this->_userModel->isNicknameUnique($values['email'])){
			$form->addError("Email již existuje");
		} else {
			$settings = [];
			$student = [];
			foreach ($values as $key => $value) {
				if(Strings::startsWith($key, 'settings_')) {
					$settings[str_replace('settings_', '', $key)] = $value ;
					unset($values[$key]);
				} else if(Strings::startsWith($key, 'student_')) {
					$student[str_replace('student_', '', $key)] = $value ;
					unset($values[$key]);
				}
			}

			$accountID = $this->_userModel->addAccount(\App\Helper\ArrayUtil::convertArrayHashToArray($values));
			$student['account_id'] = $accountID;

			$this->_accountSettingsModel->addAccountSettings($accountID, $settings);
			$this->_studentModel->addStudent($student);

		}
	}

	/**
	 * @param Form $form
	 * @param \Nette\Utils\ArrayHash $values
	 */
	public function editFormSucceed(Form $form, $values)
	{
		if(($this->_user->nickname !== $values['nickname']) && (!$this->_userModel->isNicknameUnique($values['nickname']))){
			$form->addError("Jméno již existuje");
		} else {
			$settings = [];
			$student = [];
			foreach ($values as $key => $value) {
				if(Strings::startsWith($key, 'settings_')) {
					$settings[str_replace('settings_', '', $key)] = $value ;
					unset($values[$key]);
				} else if(Strings::startsWith($key, 'student_')) {
					$student[str_replace('student_', '', $key)] = $value ;
					unset($values[$key]);
				}
			}

			$IdAccount = $this->_user->getId();
			$this->_userModel->updateAccount($IdAccount, \App\Helper\ArrayUtil::convertArrayHashToArray($values));

			$IdAccountSettings = $this->_accountSettingsModel->getIdAccountSettingsByIdAccount($IdAccount);
			if($IdAccountSettings) {
				$this->_accountSettingsModel->updateAccountSettings($IdAccountSettings, $settings);
			}

			$IdStudent = $this->_studentModel->getIdStudentByIdAccount($IdAccount);
			if($IdStudent) {
				$this->_studentModel->editStudent($IdStudent, $student);
			}
		}
	}
}