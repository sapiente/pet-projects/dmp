<?php


namespace App\Forms;
use App\Model\AccountModel;
use App\Model\AccountSettingsModel;
use App\Model\TeacherModel;
use App\Model\UserManager;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Security\User;
use Nette\Utils\Strings;


class TeacherFormFactory extends AccountFormFactory
{
	/** @var UserManager */
	protected $_userModel;
	/** @var AccountSettingsModel $_accountSettingsModel */
	protected $_accountSettingsModel;
	/** @var TeacherModel $_teacherModel */
	protected $_teacherModel;
	/** @var  User $_user */
	protected $_user;

	/**
	 * AccountFormFactory constructor.
	 * @param User $user
	 * @param AccountModel $userModel
	 * @param AccountSettingsModel $accountSettingsModel
	 * @param TeacherModel $teacherModel
	 */
	public function __construct(User $user, AccountModel $userModel, AccountSettingsModel $accountSettingsModel,  TeacherModel $teacherModel )
	{
		$this->_user = $user;
		$this->_userModel = $userModel;
		$this->_accountSettingsModel = $accountSettingsModel;
		$this->_teacherModel = $teacherModel;
	}

	/**
	 * Create form with all possibly inputs.
	 * @return Form
	 */
	protected function createFullForm()
	{
		$form = new Form();
		$form->addText('email', 'Email')
			->setRequired('Uživatelské jméno je povinné!');
		$form->addText('password', 'Heslo')
			->setType("password")
			->setRequired('Heslo je povinné!')
			->addCondition(Form::MIN_LENGTH, 6);
		$form->addText("teacher_name", "Jméno")
			->setRequired('Jméno je povinné');
		$form->addText("teacher_surname", "Přijmení")
			->setRequired('Přijmení je povinné');
		$form->addText("teacher_email", "Email")
			->setType("email");
		$form->addText('role','')
			->setType('hidden')
			->setValue('teacher');
		$form->addCheckbox('settings_send_email', 'Emailem');
		$form->addCheckbox('settings_send_notification', 'Na účet');

		return $form;
	}

	/**
	 * Create registration form
	 * @return Form
	 */
	public function createInsertForm()
	{
		$form = $this->createFullForm();
		$form->onSuccess[] = array($this, 'insertFormSucceeded');

		return $form;
	}

	/**
	 * Create edit form
	 * @return Form
	 */
	public function createEditForm()
	{
		$form = $form = $this->createFullForm();
		$form->onSuccess[] = array($this, 'editFormSucceeded');

		return $form;
	}

	public function createEditFormForTeacher()
	{
		$form = new Form();
		$form->addText('password', 'Heslo')
			->setType("password")
			->setRequired('Heslo je povinné!')
			->addCondition(Form::MIN_LENGTH, 6);
		$form->addText("teacher_email", "Email")
			->setType("email");
		$form->addCheckbox('settings_send_email', 'Emailem');
		$form->addCheckbox('settings_send_notification', 'Na účet');
		$form->onSuccess[] = array($this, 'editFormSucceeded');

		return $form;
	}



	/**
	 * @param Form $form
	 * @param \Nette\Utils\ArrayHash $values
	 */
	protected function insertFormSucceeded(Form $form, $values)
	{

		if(!$this->_userModel->isNicknameUnique($values['nickname'])){
			$form->addError("Jméno již existuje");
		} else {
			$settings = [];
			$teacher = [];
			foreach ($values as $key => $value) {
				if(Strings::startsWith($key, 'settings_')) {
					$settings[str_replace('settings_', '', $key)] = $value ;
					unset($values[$key]);
				} else if(Strings::startsWith($key, 'teacher_')) {
					$teacher[str_replace('teacher_', '', $key)] = $value ;
					unset($values[$key]);
				}
			}

			$accountID = $this->_userModel->addAccount(\App\Helper\ArrayUtil::convertArrayHashToArray($values));
			$teacher['account_id'] = $accountID;

			$this->_accountSettingsModel->addAccountSettings($accountID, $settings);
			$this->_teacherModel->addTeacher($teacher);

		}
	}


	/**
	 * @param Form $form
	 * @param \Nette\Utils\ArrayHash $values
	 */
	protected function editFormSucceeded(Form $form, $values)
	{
		if(($this->_user->getIdentity()->nickname !== $values['nickname']) && (!$this->_userModel->isNicknameUnique($values['nickname']))){
			$form->addError("Jméno již existuje");
		} else {
			$settings = [];
			$teacher = [];
			foreach ($values as $key => $value) {
				if(Strings::startsWith($key, 'settings_')) {
					$settings[str_replace('settings_', '', $key)] = $value ;
					unset($values[$key]);
				} else if(Strings::startsWith($key, 'student_')) {
					$teacher[str_replace('teacher_', '', $key)] = $value ;
					unset($values[$key]);
				}
			}

			$IdAccount = $this->_user->getId();
			$this->_userModel->updateAccount($IdAccount, \App\Helper\ArrayUtil::convertArrayHashToArray($values));

			$IdAccountSettings = $this->_accountSettingsModel->getIdAccountSettingsByIdAccount($IdAccount);
			if($IdAccountSettings) {
				$this->_accountSettingsModel->updateAccountSettings($IdAccountSettings, $settings);
			}

			$IdTeacher = $this->_teacherModel->getIdStudentByIdAccount($IdAccount);
			if($IdTeacher) {
				$this->_teacherModel->editTeacher($IdTeacher, $teacher);
			}
		}
	}

}