<?php


namespace App\Forms;


use Nette\Application\UI\Form;
use Nette\Object;

/**
 * Class LGWorkFormFactory
 * @package App\Forms
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class LGWorkFormFactory extends Object
{
	/**
	 * Create form
	 * @return Form
	 */
	public function create()
	{
		$form = new Form();

		return $form;
	}
}