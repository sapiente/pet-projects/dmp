<?php
namespace App\UserModule\Presenters;

/**
 * Class BasePresenter
 * @package App\UserModule\Presenters
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class BasePresenter extends \App\Presenters\BasePresenter
{
	public function startUp()
	{
		parent::startup();
		if(!$this->isAjax() && !$this->user->isLoggedIn()) {
			$this->redirect(':Public:Sign:in');
		}
	}
}