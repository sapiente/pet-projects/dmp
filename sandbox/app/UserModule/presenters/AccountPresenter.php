<?php


namespace App\UserModule\Presenters;
use App\Forms\AccountFormFactory;
use App\Forms\StudentFormFactory;
use App\Forms\TeacherFormFactory;
use App\Model\AccountModel;

/**
 * Class AccountPresenter
 * @package App\Presenters
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class AccountPresenter extends BasePresenter
{

	/** @var  AccountModel $_studentFormFactory */
	protected $_studentFormFactory;
	/** @var TeacherFormFactory $_teacherFormFactory */
	protected $_teacherFormFactory;
	/** @var  AccountModel $_accountModel */
	protected $_accountModel;

	/**
	 * AccountPresenter constructor.
	 * @param StudentFormFactory $studentFormFactory
	 * @param TeacherFormFactory $teacherFormFactory
	 * @param AccountModel $accountModel
	 */
	public function __construct(StudentFormFactory $studentFormFactory, TeacherFormFactory $teacherFormFactory,  AccountModel $accountModel)
	{
		$this->_studentFormFactory = $studentFormFactory;
		$this->_accountModel = $accountModel;
		$this->_teacherFormFactory = $teacherFormFactory;
	}


	public function renderRegister()
	{}

	public function renderEdit()
	{
		$data = $this->_accountModel->getAccountWithConfig($this->user->getId());
		$this->template->account = $data['account'];
		$this->template->config = $data['config'];
	}

	protected function createComponentStudentRegisterForm()
	{
		$form = $this->_studentFormFactory->createInsertForm();
		$form->onSuccess[] = function ($form, $values) {
			$this->flashMessage("Student {$values['email']} úspěšně přidán");
			$this->redirect('LGWork:showAll');
		};
		return $form;

	}

	protected function createComponentTeacherRegisterForm()
	{
		$form = $this->_teacherFormFactory->createInsertForm();
		$form->onSuccess[] = function ($form, $values) {
			$this->flashMessage("Uživatel {$values['nickname']} úspěšně přidán");
			$this->redirect('LGWork:showAll');
		};
		return $form;
	}

	/**
	 * Check unique nickname
	 * @param string $nickname
	 */
	public function actionCheckUniqueNickname($nickname)
	{
		if($this->isAjax()) {
			$this->sendJson(['result' => $this->_accountModel->isNicknameUnique($nickname)]);
		}
	}
}