<?php


namespace App\AdminModule\Presenters;

/**
 * Class BasePresenter
 * @package App\AdminModule\Presenters
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class BasePresenter extends \App\Presenters\BasePresenter
{
	public function startUp()
	{
		if(!$this->user->isLoggedIn()) {
			$this->redirect(':Public:Sign:in');
		}
	}
}