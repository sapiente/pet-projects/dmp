<?php


namespace App\Model;
use Nette\Database\Context;

/**
 * Class TeacherModel
 * @package App\Model
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class TeacherModel
{
	/** @var   \App\Model\Factory\TeacherFactory $_teacherFactory */
	protected $_teacherFactory;
	/** @var Context $_database */
	protected $_database;

	/**
	 * TeacherModel constructor.
	 * @param Factory\TeacherFactory $teacherFactory
	 * @param Context $database
	 */
	public function __construct(Factory\TeacherFactory $teacherFactory, Context $database)
	{
		$this->_teacherFactory = $teacherFactory;
		$this->_database = $database;
	}

	/**
	 * Add new teacher.
	 * @param array $data
	 * @return int Inserted id.
	 */
	public function addTeacher(array $data)
	{
		return $this->_teacherFactory->insert($data);
	}

	/**
	 * Edit specific teacher data.
	 * @param int $IdTeacher
	 * @param array $data New data.
	 * @return bool
	 */
	public function editTeacher($IdTeacher, array $data)
	{
		return $this->_teacherFactory->update($IdTeacher, $data);
	}


	/**
	 * Get teacher id byt his account id.
	 * @param int $IdAccount
	 * @return bool|mixed|\Nette\Database\Table\ActiveRow|\Nette\Database\Table\IRow
	 */
	public function getIdStudentByIdAccount($IdAccount)
	{
		return $this->_database->table(\App\Helper\DBConstants::TEACHER_TABLE)->where('account_id = ?', $IdAccount)->fetch();
	}

}