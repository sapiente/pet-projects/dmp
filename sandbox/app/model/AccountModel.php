<?php


namespace App\Model;
use App\Helper\DBConstants;
use App\Model\Factory\AccountFactory;
use Nette\Database\Context;
use Nette\Security\Passwords;

/**
 * Class AccountModel
 * @package App\Model
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class AccountModel
{
	/** @var  Context $_database */
	protected $_database;
	/** @var UserManager $_accountFactory */
	protected $_accountFactory;

	/**
	 * AccountModel constructor.
	 * @param Context $database
	 * @param AccountFactory $accountFactory
	 */
	public function __construct(Context $database, AccountFactory $accountFactory)
	{
		$this->_database = $database;
		$this->_accountFactory = $accountFactory;
	}

	/**
	 * Check if given nickname is unique.
	 * @param string $nickname
	 * @return bool
	 */
	public function isNicknameUnique($nickname)
	{
		$row = $this->_database->table(DBConstants::ACCOUNT_TABLE)->where("email = ?", $nickname)->fetch();
		return ($row === false);
	}

	/**
	 * Add new account
	 * @param array $data
	 * @return int
	 */
	public function addAccount(array $data)
	{
		$data['password'] = Passwords::hash($data['password']);
		return $this->_accountFactory->insert($data);
	}

	/**
	 * Update specific account.
	 * @param int $IdAccount ID of updated account.
	 * @param array $data New data.
	 * @return bool
	 */
	public function updateAccount($IdAccount, array $data)
	{
		$data['password'] = Passwords::hash($data['password']);
		return $this->_accountFactory->update($IdAccount, $data);
	}

	/**
	 * Get all account information include account config
	 * @param $IdAccount
	 * @return array
	 */
	public function getAccountWithConfig($IdAccount)
	{
		$output = [];
		$output['account'] = $this->_database->table(DBConstants::ACCOUNT_TABLE)->get($IdAccount);
		$output['config'] = $this->_database->table(DBConstants::ACCOUNT_SETTINGS_TABLE)->where('account_id = ?', $IdAccount)->fetch();
		return $output;
	}

}