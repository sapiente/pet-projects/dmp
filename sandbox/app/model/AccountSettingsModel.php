<?php


namespace App\Model;

use App\Model\Factory\AccountSettingsFactory;
use Nette\Database\Context;

/**
 * Class AccountSettingsModel
 * @package App\Model
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class AccountSettingsModel
{
	/** @var AccountSettingsFactory $_accountSettingsFactory */
	protected $_accountSettingsFactory;
	/** @var Context $_database */
	protected $_database;


	/**
	 * AccountSettingsModel constructor.
	 * @param AccountSettingsFactory $accountSettingsFactory
	 * @param Context $database
	 */
	public function __construct(AccountSettingsFactory $accountSettingsFactory, Context $database)
	{
		$this->_accountSettingsFactory = $accountSettingsFactory;
		$this->_database = $database;
	}

	/**
	 * Add new account settings.
	 * @param int $IdAccount
	 * @param array $data
	 * @return int Inserted id.
	 */
	public function addAccountSettings($IdAccount, array $data)
	{
		$data['account_id'] = $IdAccount;
		return $this->_accountSettingsFactory->insert($data);
	}

	/**
	 * Update specific account settings.
	 * @param int $IdAccountSettings
	 * @param array $data New data
	 * @return bool
	 */
	public function updateAccountSettings($IdAccountSettings, array $data)
	{
		return $this->_accountSettingsFactory->update($IdAccountSettings, $data);
	}


	/**
	 * Get ID account settings by given ID account
	 * @param int $IdAccount
	 * @return bool|mixed|\Nette\Database\Table\ActiveRow|\Nette\Database\Table\IRow
	 */
	public function getIdAccountSettingsByIdAccount($IdAccount)
	{
		return $this->_database->table(\App\Helper\DBConstants::ACCOUNT_SETTINGS_TABLE)->where('account_id = ?', $IdAccount)->fetch();
	}
}