<?php


namespace App\Model\Factory;

/**
 * Class StudentFactory
 * @package App\Model\Factory
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class StudentFactory extends Factory
{

	/**
	 * Initialize table name for base defined methods.
	 * @return mixed
	 */
	protected function initializeTableName()
	{
		$this->_tableName = \App\Helper\DBConstants::STUDENT_TABLE;
	}
}