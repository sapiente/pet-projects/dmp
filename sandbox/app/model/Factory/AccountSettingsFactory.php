<?php


namespace App\Model\Factory;
use App\Helper\DBConstants;

/**
 * Class AccountSettingsFactory
 * @package App\Model\Factory
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class AccountSettingsFactory extends Factory
{

	/**
	 * Initialize table name for base defined methods.
	 * @return mixed
	 */
	protected function initializeTableName()
	{
		$this->_tableName = DBConstants::ACCOUNT_SETTINGS_TABLE;
	}
}