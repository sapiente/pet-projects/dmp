<?php


namespace App\Model\Factory;
use App\Helper\DBConstants;

/**
 * Class AccountFactory
 * @package App\Model\Factory
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class AccountFactory extends Factory
{
	/**
	 * Initialize table name for base defined methods.
	 * @return mixed
	 */
	protected function initializeTableName()
	{
		$this->_tableName = DBConstants::ACCOUNT_TABLE;
	}
}