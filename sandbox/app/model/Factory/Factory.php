<?php


namespace App\Model\Factory;

/**
 * Abstract Class Factory
 * @package App\Model\Factory
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
abstract class Factory
{
	/** @var  \Nette\Database\Context $_database */
	protected $_database;
	/** @var  String $_tableName */
	protected $_tableName;

	/**
	 * Factory constructor.
	 * @param \Nette\Database\Context $database
	 */
	public function __construct(\Nette\Database\Context $database)
	{
		$this->_database = $database;
		$this->initializeTableName();
	}

	/**
	 * Initialize table name for base defined methods.
	 * @return mixed
	 */
	abstract protected function initializeTableName();

	/**
	 * Insert into table given data.
	 * @param array $data Data to be inserted.
	 * @return int Inserted id
	 */
	public function insert(array $data)
	{
		$this->checkDefinedTableName();
		$row =  $this->_database->table($this->_tableName)->insert($data);
		return $row->id;
	}

	/**
	 * Update row by given id with given data.
	 * @param int $ID ID of row to be updated.
	 * @param array $data New data.
	 * @return boolean
	 */
	public function update($ID, array $data)
	{
		$this->checkDefinedTableName();
		$result =  $this->_database->table($this->_tableName)
			->where('id = ?', $ID)
			->update($data);
		return ($result !== false);
	}

	/**
	 * Update all rows with given data.
	 * @param array $data New dat.
	 * @return boolean
	 */
	public function updateAll(array $data)
	{
		$this->checkDefinedTableName();
		$result =  $this->_database->table($this->_tableName)
			->update($data);
		return ($result !== false);
	}

	/**
	 * Delete row by given ID.
	 * @param int $ID ID of deleted row.
	 * @return boolean
	 */
	public function delete($ID)
	{
		$this->checkDefinedTableName();
		$result =  $this->_database->table($this->_tableName)
			->where('id = ?', $ID)
			->delete();
		return ($result !== false);
	}

	/**
	 * Check if is defined table name for base CRUD methods.
	 * @throws UndefinedTableNameException
	 */
	private function checkDefinedTableName() {
		if(empty($this->_tableName)) {
			throw new UndefinedTableNameException;
		}
	}
}

/**
 * Class UndefinedTableNameException
 * @package App\Model\Factory
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class UndefinedTableNameException extends \Exception
{}