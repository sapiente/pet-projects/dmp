<?php


namespace App\Model\Factory;

/**
 * Class TeacherFactory
 * @package App\Model\Factory
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class TeacherFactory extends Factory
{

	/**
	 * Initialize table name for base defined methods.
	 * @return mixed
	 */
	protected function initializeTableName()
	{
		$this->_tableName = \App\Helper\DBConstants::TEACHER_TABLE;
	}
}