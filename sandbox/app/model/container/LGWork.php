<?php


class LGWork
{
	/** @var  string */
	public $title;
	/** @var  string */
	public $language;
	/** @var  string DATE format */
	public $post_date;

	/**
	 * LGWork constructor.
	 * @param string $language
	 * @param string $post_date
	 * @param string $title
	 */
	public function __construct($language, $post_date, $title)
	{
		$this->language = $language;
		$this->post_date = $post_date;
		$this->title = $title;
	}

}