<?php
use Nette\Database\Table\IRow;
use Nette\Object;

/**
 * Class LGWorksContainer
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class LGWorksContainer extends Object
{

	/** @var LGWork[] */
	public $LGWorks = [];

	/**
	 * Save LG works from IRows
	 * @param array $rows
	 */
	public function saveLGWorksFromIRows(array $rows)
	{
		foreach ($rows as $row) {
			if($row instanceof IRow) {
				$this->LGWorks[] = new LGWork(
					$row->language, $row->post_date->format('j.n.Y'), $row->title
				);
			} else {
				if(is_object($row)) {
					trigger_error(get_class($row) . " is not instance of IRow");
				} else {
					trigger_error(" Row is not instance of IRow");
				}
			}
		}

	}

	/**
	 * Save LG works
	 * @param array $works
	 */
	public function saveLGWorks(array $works)
	{
		foreach ($works as $work) {
			if($work instanceof LGWork) {
				$this->LGWorks[] = $work;
			} else {
				if(is_object($work)) {
					trigger_error(get_class($work) . " is not instance of IRow");
				} else {
					trigger_error(" Row is not instance of LGWork");
				}
			}
		}
	}

	/**
	 * @return LGWork[]
	 */
	public function getLGWorks()
	{
		return $this->LGWorks;
	}


}