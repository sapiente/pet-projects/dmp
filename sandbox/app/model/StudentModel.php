<?php


namespace App\Model;

use App\Model\Factory\StudentFactory;
use Nette\Database\Context;

/**
 * Class StudentModel
 * @package App\Model
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class StudentModel
{
	/** @var StudentFactory $_studentFactory */
	protected $_studentFactory;
	/** @var Context $_database */
	protected $_database;

	/**
	 * StudentModel constructor.
	 * @param StudentFactory $studentFactory
	 * @param Context $database
	 */
	public function __construct(StudentFactory $studentFactory, Context $database)
	{
		$this->_studentFactory = $studentFactory;
		$this->_database = $database;
	}

	/**
	 * Add new student.
	 * @param array $data
	 * @return int
	 */
	public function addStudent(array $data)
	{
		return $this->_studentFactory->insert($data);
	}

	/**
	 * Edit specific student data.
	 * @param int $IdStudent
	 * @param array $data New data.
	 * @return bool
	 */
	public function editStudent($IdStudent, array $data)
	{
		return  $this->_studentFactory->update($IdStudent, $data);
	}


	/**
	 * Get student id byt his account id.
	 * @param int $IdAccount
	 * @return bool|mixed|\Nette\Database\Table\ActiveRow|\Nette\Database\Table\IRow
	 */
	public function getIdStudentByIdAccount($IdAccount)
	{
		return $this->_database->table(\App\Helper\DBConstants::STUDENT_TABLE)->where('account_id = ?', $IdAccount)->fetch();
	}

}