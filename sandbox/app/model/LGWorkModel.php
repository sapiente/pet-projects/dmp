<?php


namespace App\Model;
use App\Helper\DBConstants;
use Nette\Database\Context;

/**
 * Class LGWorkModel
 * @package App\Model
 * @author Pavel Parma <pavel.parma@mobilbonus.cz>
 */
class LGWorkModel
{
	/** @var Context */
	protected $_database;

	/**
	 * LGWorkModel constructor.
	 * @param Context $_database
	 */
	public function __construct(Context $_database)
	{
		$this->_database = $_database;
	}


	/**
	 * @param int $year Filter by given year (YYYY)
	 * @return array|\Nette\Database\Table\IRow[]
	 */
	public function getAllWork($year = null)
	{
		$select = $this->_database->table(DBConstants::LGWORK_TABLE);
		if(!is_null($year)){
			$select->where("YEAR(`post_date`) = ?", $year);
		}

		return $select->fetchAll();
	}

	/**
	 * Get LG work container with works
	 * @param int $year Filter by given year
	 * @return \LGWorksContainer
	 */
	public function getLGWorkContainerWithWorks($year = null)
	{
		$works = $this->getAllWork($year);
		$container = new \LGWorksContainer();
		$container->saveLGWorksFromIRows($works);
		return $container;
	}
}