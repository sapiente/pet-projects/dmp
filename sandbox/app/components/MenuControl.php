<?php


namespace App\Components;


use Nette\Security\User;

class MenuControl extends \Nette\Application\UI\Control
{
	/** @var User $_user */
	protected $_user;

	/**
	 * MenuControl constructor.
	 * @param User $user
	 */
	public function __construct(User $user)
	{
		$this->_user = $user;
	}

	public function render()
	{
//		$this->link('LGWork:showAll');
		$template = $this->template;
		$fileName = 'unlogged';
		if($this->_user->isLoggedIn()) $fileName = $this->_user->identity->role;
		$template->setFile(__DIR__ . "/templates/Menu/$fileName.latte");
		$template->user = $this->_user;
		$template->render();
	}

}