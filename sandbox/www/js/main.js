var URL = 'http://localhost/DMP/sandbox/www';

/**
 * Get long graduation works in year
 * @param year int
 */
function getLGWorks(year) {
    var url = document.location.protocol + '//' +
        document.location.host +
        document.location.pathname +
        "?year=" + year;
    $.post(url, function(data) {
        console.log(data);
        $(".lgwork").each(function(){
            $(this).remove();
        });
        $.each(data, function() {
            $("#lgworks").append(
                "<tr class='lgwork'><td>"+this.title+"</td><td>"+this.language+"</td><td>"+this.post_date+"</td></tr>"
            );
        });
    });

    $("#year").text(year);
}

/**
 * Check if given nickname is not already used.
 * @param input Object
 */
function checkUniqueNickname(input) {
    var url = document.location.protocol + '//' +
        document.location.host +
        "/check/nickname/" + $(input).val();
    $.post(url, function(data) {
        if(data.result) {
            $(input).removeClass("inputError");
        } else {
            $(input).addClass("inputError");
            $(input).closest("form").find("ul.errors").append("<li>U�ivatelsk� jm�no nen� unik�tn�</li>");
            console.log($(input).closest("form").find(".errors"));
        }
    });
}